//Librerías
const express = require('express')
const app = express()
const https = require('https')
const http = require('http').Server(app)
const io = require('socket.io')(http, https)
const db = require('mongoose')
const jwt = require('jsonwebtoken')
const cookieParser = require('cookie-parser')
const parser = require('body-parser')
const moment = require('moment')
const multer = require('multer')
const request = require('request')
const cheerio = require('cheerio')
const CronJob = require('cron').CronJob
const fs = require('fs')
//Certificado para HTTPS
const privateKey  = fs.readFileSync('./private.key', 'utf8');
const certificate = fs.readFileSync('./certificate.crt', 'utf8');
//Archivo de configuración
const config = require('./config.js')

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

//Conexión con MongoDB
db.Promise = global.Promise
db.connect(config.database, {useNewUrlParser: true}, function(error) {
  if(error) {
    console.log('Error MongoDB:', error);
  }
});
const MongoDB = db.connection
const ObjectId = require('mongodb').ObjectID

//Configuración del servidor Express
app.use(express.static(__dirname + '/Public'))
app.use(parser())
app.use(cookieParser())
app.use(express.json())

const api = express.Router()

//Middleware para validar los tokens en las API's
//El token se envía en la cabecera 'auth: Bearer {{token}}'
api.use(function(req, res, next) {
  try {
    var token = req.headers.auth.split(' ')[1]
    //var token = req.body.token || req.query.token || req.headers['x-access-token'];
    jwt.verify(token, config.secret, function(error, decoded) {
      if (error) {
        return res.json({ msg: 'error', data: 'Autorization_error' })
      }else{
        req.token = decoded
        next()
      }
    })
  }catch(e) {
    return res.status(403).json({msg: 'error', data: 'Autorization_required' })
  }
})

//Agregar Middleware para todas las rutas que empiezan con '/api'
app.use('/api', api)

//API REST (Utilizar Postman para las pruebas)

//API login
app.post('/login', function(req, res) {
  var usuario = req.body.usuario.trim()
  var password = req.body.password.trim()

  //ALgoritmo para calcular la fecha de mañana a las 12:00 am, es decir, el token vence todos los días a las 11:59:59pm
  var m = moment().hour(0).minute(0).second(0).millisecond(0)
  m = m.add(1, 'days')
  var ms = moment(m).diff(moment())
  var r = moment.duration(ms)
  var restante = Math.floor(r / 1000)

  //Busco en la colección 'usuarios' si alguno tiene un 'usuario' y una 'contraseña' igual a la que nos enviaron
  MongoDB.collection('usuarios').findOne({'usuario': usuario, 'contraseña': password}, function(error, find){
    if(error) {
      res.json({msg: 'error', data: 'Internal_error'})
    }else if(find){
      res.json({msg: 'ok', token: jwt.sign({user: find._id}, config.secret, { expiresIn: restante })})
    }else{
      res.json({msg: 'error', data: 'not_found'})
    }
  })
})

//Configuracion previa antes de subir el archivo del convenio empresarial
var storageConvenio = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'Public/assets/archivos') // La ruta donde almacenará el archivo
  },
  filename: function (req, file, cb) { // Usa el nombre con el siguiente formato: nombre_original + "-" + fecha_actual + "." + extension
    var name = file.originalname.split('.');
    var extension = name[name.length-1]
    cb(null, file.fieldname + '-' + Date.now() + '.' + extension)
  }
})

var convenio = multer({ storage: storageConvenio })

//API registrar convenio empresarial
app.post('/convenios_empresariales/registro', function(req, res) {
  /*
  Ejemplo de json recibido
  {
  	"razon_social": "Sociedad Eléctrica del Sur Oeste S. A.",
  	"nombre": "SEAL",
  	"ruc": "20100188628",
  	"descripcion": "Generación y Dist. Energia Eléctrica.",
  	"sector": "Empresa del Estado",
  	"domicilio_fiscal": "Cal. Consuelo Nro. 310",
  	"distrito": "Arequipa",
  	"ciudad": "Arequipa",
  	"departamento": "Arequipa",
  	"telefonos": [
  		{
  			"cod_pais": "+51",
  			"cod_ciudad": "054",
  			"numero": "381200",
  			"anexo": "123"
  		}
  	],
  	"correo": "info@seal.com.pe",
  	"web": "http://www.seal.com.pe",
  	"contactos": [
  		{
  			"nombre": "Juan Perez",
  			"telefono": {
          "cod_pais": "+51",
  				"cod_ciudad": "054",
  				"numero": "381200",
  				"anexo": "123"
        },
  			"correo": "juan@seal.com.pe"
  		}
  	],
    "fecha_inicio": 1583539576,
  	"fecha_fin": 1583539495
  }
  */

  //Almaceno el json recibido en la variable 'data'
  var data = req.body

  // Valido que tenga 'razon_social' y 'ruc' como requisito indispensable para proceder con el registro
  if(!data.razon_social || !data.ruc) { //Si no tiene alguno de los dos, error
    res.json({msg: 'error', data: 'Invalid_request'})
  }else{
    //Creo un nuevo json y le voy pasando los valores de 'data'
    var json = {
      razon_social: data.razon_social.trim(),
      nombre: data.nombre ? data.nombre.trim() : null,
    	ruc: data.ruc.trim(),
    	descripcion: data.descripcion ? data.descripcion.trim() : null,
    	sector: data.sector ? data.sector.trim() : null,
    	domicilio_fiscal: data.domicilio_fiscal ? data.domicilio_fiscal.trim() : null,
    	distrito: data.distrito ? data.distrito.trim() : null,
    	ciudad: data.ciudad ? data.ciudad.trim() : null,
    	departamento: data.departamento ? data.departamento.trim() : null,
    	telefonos: [],
    	correo: data.correo ? data.correo.trim() : null,
    	web: data.web ? data.web.trim() : null,
    	contactos: []
    }

    //Valido que tenga 'fecha_inicio' y 'fecha_fin' para poder agregarlos en el json, de lo contrario, no es necesario que aparezcan
    if(data.fecha_inicio && data.fecha_fin) {
      json.archivos = []
    	json.fecha_inicio = data.fecha_inicio
    	json.fecha_fin = data.fecha_fin
    }

    for(var i = 0; i < data.telefonos.length; i++) {
      var telefonos = data.telefonos
      if(telefonos[i].numero) {
        json.telefonos.push(
          {
            cod_pais: telefonos[i].cod_pais ? telefonos[i].cod_pais.trim() : null,
      			cod_ciudad: telefonos[i].cod_ciudad ? telefonos[i].cod_ciudad.trim() : null,
      			numero: telefonos[i].numero,
      			anexo: telefonos[i].anexo ? telefonos[i].anexo.trim() : null
          }
        )
      }
    }

    for(var i = 0; i < data.contactos.length; i++) {
      var contactos = data.contactos
      if(contactos[i].nombre) {
        json.contactos.push(
          {
            nombre: contactos[i].nombre.trim(),
      			telefono: {
              cod_pais: contactos[i].telefono.cod_pais ? contactos[i].telefono.cod_pais.trim() : null,
        			cod_ciudad: contactos[i].telefono.cod_ciudad ? contactos[i].telefono.cod_ciudad.trim() : null,
        			numero: contactos[i].telefono.numero,
        			anexo: contactos[i].telefono.anexo ? contactos[i].telefono.anexo.trim() : null
            },
            correo: contactos[i].correo ? contactos[i].correo.trim() : null
          }
        )
      }
    }

    //Inserto la variable 'json' en la colección 'convenios_empresariales' de MongoDB
    MongoDB.collection('convenios_empresariales').insert(json, function(error, inserted) {
      if(error) {
        res.json({msg: 'error', data: 'Internal_error'})
      }else{
        res.json({msg: 'ok', id: inserted.insertedIds[0]})
      }
    })
  }
})

//API subir archivo de convenio empresarial
app.post('/convenios_empresariales/subir_archivo/:id', convenio.single('file'), (req, res, next) => {
  // Recibe el id de la empresa y el archivo a subir
  var id = req.params.id
  var file = req.file
  //Si no recibo ningún archivo
  if (!file) {
    res.json({msg: 'error', data: 'Por favor, sube un archivo'})
  }else{
    //Busco una empresa en la colección 'convenios_empresariales' que coincida con el '_id' recibido
    MongoDB.collection('convenios_empresariales').findOne({ '_id': db.Types.ObjectId(id) }, function (error, find) {
      if(error) {
        res.json({msg: 'error', data: error})
      }else{
        if(!find.archivos) {
          find.archivos = []
        }
        find.archivos.push({
          img: IconoArchivo('../assets/archivos/' + file.filename),
          ruta: '../assets/archivos/' + file.filename,
          iat: moment().unix()
        })
        //Actualizo los datos en MongoDB
        MongoDB.collection('convenios_empresariales').update({'_id': db.Types.ObjectId(id)}, find, function(error, updated) {
          if(error) {
            res.json({msg: 'error', error: error})
          }else{
            res.json({msg: 'ok', img: find.archivos[find.archivos.length-1].img, ruta: find.archivos[find.archivos.length-1].ruta})
          }
        })
      }
    })
  }
})

//Función para buscar el ícono correspondiente al tipo de archivo
function IconoArchivo(name) {
  name = name.toLowerCase()
  if(name.endsWith('.jpg') || name.endsWith('.jpeg') || name.endsWith('.jpe') || name.endsWith('.jif') || name.endsWith('.jfif') || name.endsWith('.jfif') ||
     name.endsWith('.png') || name.endsWith('.gif') || name.endsWith('.webp') || name.endsWith('.tiff') || name.endsWith('.tif') || name.endsWith('.bmp')) {
       return name
  }else if(name.endsWith('.pdf')) {
    return '../assets/img/pdf.png'
  }else if(name.endsWith('.doc') || name.endsWith('.docx')) {
    return '../assets/img/word.png'
  }else if(name.endsWith('.xls') || name.endsWith('.xlsx') || name.endsWith('.csv')) {
    return '../assets/img/excel.png'
  }else{
    return '../assets/img/archivo.png'
  }
}

//API leer convenios empresariales
app.get('/convenios_empresariales', function(req, res) {
  //Busca todos los convenios_empresariales odenados alfanuméricamente por 'nombre'
  MongoDB.collection('convenios_empresariales').find({})
    .sort({
      nombre: 1
    }).toArray(function(error, find) {
      if(error) {
        res.json({msg: 'error', error: 'Internal_error'})
      }else if(find.length > 0) {
        res.json({msg: 'ok', data: find})
      }else{
        res.json({msg: 'ok', data: []})
      }
    })
})

//API eliminar un convenio empresarial
app.post('/convenios_empresariales/eliminar', function(req, res) {
  var id = req.body.id
  MongoDB.collection('convenios_empresariales').remove({ '_id' : db.Types.ObjectId(id)}, function(error, removed) {
    if(error) {
      res.json({msg: 'error', data: 'Internal_error'})
    }else{
      res.json({msg: 'ok', data: id})
    }
  })
})

/***********************INICIO IGNORAR***********************/
app.get('/empleos', function(req, res) {
  var query = req.query.q
  res.json({
    msg: 'ok',
    data: [
      {
        id: 1,
        titulo: '<h5>JEFE DE LA UNIDAD DE ADMINISTRACIÓN TRIBUTARIA</h5>',
        img: 'https://lh3.googleusercontent.com/proxy/QwV3eTA2zZQt57z2O3zQpV7HdUz6oJ_RfIIyznI7UIH1SgLipj64x6A8zvZOMhnArzh0EDicOWrHMCy2OLLZnJrEcKAl0dTWCcs9B-U4f-TTzkQy-lt-kAKcf01zj2Rafxzm9Yt7Ll50',
        entidad: 'Ministerio de Vivienda, Construcción y Saneamiento',
        fecha: '05/03/2020 - 13/03/2020',
        ubicacion: 'AREQUIPA - SAN JUAN DE SIGUAS',
        remuneracion: 'S/. 1,700.00',
        vacantes: '1 vacante'
      }
    ]
  })
})

app.get('/empleos/:id', function(req, res) {
  var id = req.params.id
  res.json({
    msg: 'ok',
    data: {
      titulo: 'JEFE DE LA UNIDAD DE ADMINISTRACIÓN TRIBUTARIA',
      img: 'https://lh3.googleusercontent.com/proxy/QwV3eTA2zZQt57z2O3zQpV7HdUz6oJ_RfIIyznI7UIH1SgLipj64x6A8zvZOMhnArzh0EDicOWrHMCy2OLLZnJrEcKAl0dTWCcs9B-U4f-TTzkQy-lt-kAKcf01zj2Rafxzm9Yt7Ll50',
      entidad: 'Ministerio de Vivienda, Construcción y Saneamiento',
      codigo: 'D. LEG 1057 - 001',
      requisitos: [
        'EXPERIENCIA: MÍNIMA DE TRES (03) AÑOS CONTADOS DESDE EL GRADO DE BACHILLER, EN SECTOR PRIVADO Y/O PÚBLICO. (ACREDITADO) Y MÍNIMA DE UN (01) AÑO DESEMPEÑANDO FUNCIONES AFINES AL CARGO.',
        'FORMACIÓN ACADÉMICA - PERFIL: TÍTULO PROFESIONAL EN ADMINISTRACIÓN, ECONOMÍA Y CONTABILIDAD, CON COLEGIATURA Y HABILITACIÓN VIGENTE.'
      ],
      detalles: [
        '<a href="https://muniprovincialcotabambas.gob.pe/" target="_black">https://muniprovincialcotabambas.gob.pe/</a>'
      ],
      fecha: '05/03/2020 - 13/03/2020',
      ubicacion: 'AREQUIPA - SAN JUAN DE SIGUAS',
      remuneracion: 'S/. 1,700.00',
      vacantes: 1
    }
  })
})

//Web Scraping a Servir
app.get('/servir', function(req, res) { //Ejemplo: localhost:3000/servir

  var url = 'https://app.servir.gob.pe/DifusionOfertasExterno/faces/consultas/ofertas_laborales.xhtml'

  console.log('Consultado URL -->', url);

  request({
    headers: {
      'Connection': 'keep-alive',
      'Upgrade-Insecure-Requests': '1',
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36',
      'Sec-Fetch-Dest': 'document',
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
      'Sec-Fetch-Site': 'none',
      'Sec-Fetch-Mode': 'navigate',
      'Accept-Language': 'es-ES,es;q=0.9'
    },
    uri: url,
    method: 'GET'
  }, function(error, response, html) {
    if(error) {
      res.json({msg : 'error', type : error})
    }else{

      var $ = cheerio.load(html)
      var json = []

      var end = $('.titulo-vacante').find('label').toArray().length

      if(end == 0) {
        res.json({msg : 'ok', data : json})
      }else{
        $('.titulo-vacante').find('label').each(function (index, element) {

          var detalle_sp = $(element).parent().parent().parent().find('span[class="detalle-sp"]')

          var title = $(element).text().trim()
          if(title.length <= 50) {
            title = '<h5>' + title + '</h5>'
          }else if(title.length < 75) {
            title = '<h6>' + title + '</h6>'
          }else{
            title = '<h6>' + title.substr(0, 75) + '...</h6>'
          }
          var img = $(element).parent().parent().parent().find('img').attr('src').replace('../', 'https://app.servir.gob.pe/DifusionOfertasExterno/faces/')

          var vacantes = (detalle_sp.eq(3).text().replace(/\t/g, '').trim()).replace(/\n/g, ' ') + ' vacante'
          if(vacantes != '1 vacante') {
            vacantes += 's'
          }

          json.push(
            {
              titulo: title,
              img: img,
              entidad: (detalle_sp.eq(0).text().replace(/\t/g, '').trim()).replace(/\n/g, ' '),
              ubicacion: (detalle_sp.eq(1).text().replace(/\t/g, '').trim()).replace(/\n/g, ' '),
              codigo: (detalle_sp.eq(2).text().replace(/\t/g, '').trim()).replace(/\n/g, ' '),
              vacantes: vacantes,
              remuneracion: (detalle_sp.eq(4).text().replace(/\t/g, '').trim()).replace(/\n/g, ' '),
              fecha: (detalle_sp.eq(5).text().replace(/\t/g, '').trim()).replace(/\n/g, ' ') + ' - ' + (detalle_sp.eq(6).text().replace(/\t/g, '').trim()).replace(/\n/g, ' ')
          })

          if(index == end-1) {
            res.json({msg : 'ok', data : json})
          }
        })
      }
    }
  })
})

//Web Scraping a CompuTrabajo
app.get('/computrabajo', function(req, res) { //Ejemplo: localhost:3000/computrabajo?query=Informatica&location=aqp&page=1
  var query = req.query.query
  var location = req.query.location
  var page = req.query.page ? req.query.page : '1'

  var url = 'https://www.computrabajo.com.pe/'

  switch (location) {
    case 'aqp':
      url += 'empleos-en-arequipa?p=' + page + '&q=' + String(query).trim()
      break
  }

  console.log('Consultado URL -->', url);

  request(url, function(error, response, html) {
    if(error) {
      res.json({msg : 'error', type : error})
    }else{

      var $ = cheerio.load(html)
      var json = []

      var end = $('div[id="p_ofertas"]').find('div').find('h2').find('a').toArray().length

      if(end == 0) {
        res.json({msg : 'ok', data : json})
      }else{
        $('div[id="p_ofertas"]').find('div').find('h2').find('a').each(function (index, element) {

          var title = $(element).text().trim()
          var description = $(element).parent().parent().find('p').text().trim()
          var company = $(element).parent().parent().find('.w_100').find('span').eq(0).find('span').eq(0).find('a').eq(0).text() != '' ? $(element).parent().parent().find('.w_100').find('span').eq(0).find('span').eq(0).find('a').eq(0).text().trim() : $(element).parent().parent().find('.w_100').find('span').eq(0).find('span').eq(0).text().trim()
          var img = $(element).parent().parent().parent().find('img').attr('data-original')

          json.push(
            {
              titulo: title,
              descripcion: description,
              empresa: company,
              img: img
          })

          if(index == end-1) {
            res.json({msg : 'ok', data : json})
          }
        })
      }
    }
  })
})
/***********************FIN IGNORAR***********************/

//SocketIO (Tiempo Real)
io.on('connection', function(socket) {
  socket.on('get', function(json) {
    socket.emit('get', {msg: 'ok'})
  })

  socket.on('disconnect', function () {
    console.log('Cliente desconectado:', socket.id)
  })
})

//Tareas programadas
new CronJob('00 00 00 * * *', function() { //Todos los días a las 12:00 am (ss mm hh DD MM AAAA)
  // Hacer algo
}, null, true, 'America/Lima'); //America/Lima es la zona horaria

//Server HTTPS (Puerto 443 por defecto)
var credentials = {key: privateKey, cert: certificate};
var httpsServer = https.createServer(credentials, app).listen(config.httpsPort);

//Server HTTP (Puerto 80 por defecto)
http.listen(config.httpPort, function() {
  console.log('Servidor montado *:' + config.httpPort)
})
